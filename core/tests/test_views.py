from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from core.views import index, signup


class TestViews(TestCase):

    def setUp(self):
        self.response = self.client.get(reverse('index'))
        self.response2 = self.client.get(reverse('signup'))

    def test_index_page(self):
        self.assertTemplateUsed(self.response, 'index.html')  # Tests the html page used for the associated url
        self.assertContains(self.response, "visitor")  # Tests the page contains the word visitor

    def test_has_signup_form(self):
        form = self.response2.context['form']
        self.assertIsInstance(form, UserCreationForm) #Tests the form in the signup page is an
                                                    # instance of UserCreationForm


