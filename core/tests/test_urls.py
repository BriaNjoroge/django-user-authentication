from django.test import SimpleTestCase
from django.urls import reverse, resolve
from core.views import index, signup


class TestUrls(SimpleTestCase):
    """
    This class test the respective urls lead to their corresponding views functions
    """

    def test_index_url_is_resolved(self):
        url = reverse('index')
        self.assertEquals(resolve(url).func, index)  # Testing that the index signup does  lead to index function

    def test_accounts_url_is_resolved(self):
        url = reverse('signup')
        self.assertEquals(resolve(url).func, signup)  # Testing that the url signup does  lead to index function
        self.assertNotEquals(resolve(url).func, index)  # Testing that the url signup does not lead to index function
